package edu.sjsu.project1darwishquiambao;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.SeekBar;

import androidx.appcompat.app.AppCompatActivity;

import edu.sjsu.project1darwishquiambao.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {
    ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);
        try {
            //for initial default value only. Will reset textView after updating seekbar.
            binding.rateView.append(" 10.00%");
            binding.seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @SuppressLint("DefaultLocale")

                @Override
                public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                    double progress = (double) i/10;
                    binding.rateView.setText(String.format("%s %.2f%c",
                            getString(R.string.interestRate),
                            progress,
                            '%'));
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }
            });
            binding.calculateButton.setOnClickListener(this::calculate);
            binding.uninstallButton.setOnClickListener(this::uninstall);
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }

    @SuppressLint("DefaultLocale")
    protected void calculate(View view){
        //get principal amount from inputField;
        String str_principal = binding.principalEdit.getText().toString();
        try {
            if(!str_principal.isEmpty()){
                //validate input to have presence of zero or one decimal '.' character
                if(str_principal.matches("[0-9]*\\.?[0-9]*")) {
                    String[] str_arr = str_principal.split("\\.");
                    if (str_arr.length > 1 && str_arr[1].length() > 2) {
                        String err_msg = getString(R.string.err_decimal_dig);
                        binding.monthPayView.setText(err_msg);
                    }
                    else {
                        //get interestRate
                        double rate = (double) binding.seekBar.getProgress()/1000; //divide by 1000, offset the extra *10 factor
                        double principal = Double.parseDouble(str_principal);
                        int month = getMonth();
                        double tax = binding.taxBox.isChecked() ? 0.1 / 100 * principal : 0;
                        double result = Util.calculate(principal, rate, month, tax);
                        String message = getString(R.string.resultMsg);
                        binding.monthPayView.setText(String.format("%s %.2f", message, result));
                    }
                }
            }
            else {
                String err_msg = getString(R.string.err_empty);
                binding.monthPayView.setText(err_msg);
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }
    protected void uninstall(View view){
        Intent delete = new Intent(Intent.ACTION_DELETE, Uri.parse("package:" + getPackageName()));
        startActivity(delete);
    }

    protected int getMonth(){
        if(binding.radio15.isChecked()) return 15;
        else if(binding.radio20.isChecked()) return 20;
        else if(binding.radio30.isChecked()) return 30;
        else
            return Integer.MIN_VALUE; //returns if invalid selections not provided by UI
    }

}