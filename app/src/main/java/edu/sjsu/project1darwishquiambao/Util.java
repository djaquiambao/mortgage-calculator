package edu.sjsu.project1darwishquiambao;

import android.annotation.SuppressLint;

public class Util {
    @SuppressLint("DefaultLocale")
    public static double calculate(double principal, double intRate, int months, double tax) {

        months*=12; //Number of months of the loan (number of years * 12)
        if(intRate == 0){
             double result = principal/months + tax;
             String str = String.format("%.2f", result);
            return Double.parseDouble(str);
        }
        else{
            intRate/=12; //Monthly interest in decimal form (annual interest rate / 12)
            double temp = Math.pow(1 + intRate, -months);
            double result = principal * intRate / (1 - temp) + tax;
            String str = String.format("%.2f", result);
            return Double.parseDouble(str);
        }
    }

}
