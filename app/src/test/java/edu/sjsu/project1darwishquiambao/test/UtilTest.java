package edu.sjsu.project1darwishquiambao.test;

import junit.framework.TestCase;

import org.junit.Assert;
import org.junit.Test;

import edu.sjsu.project1darwishquiambao.Util;

public class UtilTest {

    @Test
    public void testCalculate() {
        // Test 1:
        Assert.assertEquals(81.71, Util.calculate(10000, 0.055, 15, 0), 0.005);
        // Test 2:
        Assert.assertEquals(91.71, Util.calculate(10000, 0.055, 15, 0.1/100*10000), 0.005);
        // Test 3:
        Assert.assertEquals(83.33, Util.calculate(20000, 0,20, 0), 0.005);
        // Test 4:
        Assert.assertEquals(97.76, Util.calculate(10000, 0.10, 30, 0.1/100*10000), 0.005);
        //Test 5:
        Assert.assertEquals(213.00, Util.calculate(20000, 0.1, 20, 0.1/100*20000), 0.005);
    }
}